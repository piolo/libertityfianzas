create database wsfianzas
use wsfianzas;
drop database wsfianzas

create table oficina(
id_oficina int IDENTITY(1,1) not null,
nombreEstado varchar(50)
);

create table compania(
id_compania int IDENTITY(1,1)not null,
fianza		varchar(50),
movimiento	varchar(50),
fiado		varchar(50),
antiguedad	int,
diasVencimiento int,
importe		int,
color		varchar(50),
moneda		varchar(50)
);

create table intersection(
id_oficina int,
id_compania int, 
);

select  session_id, encrypt_option FROM sys.dm_exec_connections